# ERP TEST

Project For Team Test

## Requirements

### Laravel
- Laravel version ^8

### PHP
- Recomended PHP version ^8

### Composer
- Recomended Composer version ^2.0

### Database
- Mysql

## Installation & Update

Install php dependencies
``` bash
composer install
```

Make .env
```bash
cp .env.example .env
```

Generate app key
```bash
php artisan key:generate
```

Configure DB on .env
```bash
DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=
```

Run migrations
```bash
php artisan migrate
```

### For development purpose (Optional)

Run fresh migrations with seeds
```bash
php artisan migrate:fresh --seed
```

### This step is required in every deployment

Run migrations
```bash
php artisan migrate
```


Clear/optimize system
```bash
php artisan optimize:clear
```

## Run Server

Run server
```bash
php artisan serve
```

## In This Project

Using Repository Service Pattern
### Backend

CRUD Modul Users

- API Get User
- API Detail User
- API Store User
- API Update User
- API Delete 
- Postman Collection

### Frontend
- Tailwind CSS
- DaisyUI
- Jquery
- Blade

### CDN

```bash
  <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/daisyui@1.14.2/dist/full.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/alpinejs@2.8.2/dist/alpine.min.js" defer></script>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
  <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
```

