@extends('layouts.app')
@section('content')

    <div class="flex justify-center items-center h-screen">
        <form class="bg-white p-8 rounded-lg shadow-md w-full max-w-lg" id="dataForm">
            <h2 class="text-2xl font-bold mb-6">Add Users</h2>

            <div class="mb-4">
                <label for="name" class="block text-sm font-medium text-gray-700">Name</label>
                <input type="text" id="name" name="name" class="input input-bordered w-full mt-1">
            </div>

            <div class="mb-4">
                <label for="email" class="block text-sm font-medium text-gray-700">Email</label>
                <input type="email" id="email" name="email" class="input input-bordered w-full mt-1">
            </div>

            <div class="mb-4">
                <label for="password" class="block text-sm font-medium text-gray-700">Password</label>
                <input type="password" id="password" name="password" class="input input-bordered w-full mt-1">
            </div>

            <div class="mb-4">
                <label for="address" class="block text-sm font-medium text-gray-700">Address</label>
                <input type="text" id="address" name="address" class="input input-bordered w-full mt-1">
            </div>

            <div class="mb-4">
                <label for="dob" class="block text-sm font-medium text-gray-700">Date of Birth</label>
                <input type="date" id="dob" name="dob" class="input input-bordered w-full mt-1">
            </div>

            <div class="mb-4">
                <label for="pob" class="block text-sm font-medium text-gray-700">Place of Birth</label>
                <input type="text" id="pob" name="pob" class="input input-bordered w-full mt-1">
            </div>

            <div class="mb-4">
                <label for="age" class="block text-sm font-medium text-gray-700">Age</label>
                <input type="number" id="age" name="age" class="input input-bordered w-full mt-1">
            </div>

            <div class="flex justify-end">
                <a href="{{route('user.index')}}" class="btn btn-error mr-2">Close</a>
                <button type="submit"  class="btn btn-primary" id="submitBtn">Submit</button>
            </div>
        </form>
    </div>

    <div id="loadingSpinner" class="hidden fixed inset-0 bg-gray-800 bg-opacity-50 flex justify-center items-center z-50">
        <div class="loader border-t-4 border-b-4 border-white-500 rounded-full w-12 h-12"></div>
    </div>


    <script>
        $(document).ready(function() {
            $('#dataForm').on('submit', function(event) {
                event.preventDefault();

                $('#loadingSpinner').removeClass('hidden');

                var formData = {
                    name: $('#name').val(),
                    email: $('#email').val(),
                    password: $('#password').val(),
                    address: $('#address').val(),
                    date_of_birth: $('#dob').val(),
                    place_of_birth: $('#pob').val(),
                    age: $('#age').val()
                };

                $.ajax({
                    url: 'http://127.0.0.1:8000/api/user/store',
                    type: 'POST',
                    data: JSON.stringify(formData),
                    contentType: 'application/json',
                    success: function(response) {
                        $('#loadingSpinner').addClass('hidden');
                        Swal.fire({
                            icon: 'success',
                            title: 'Success',
                            text: 'Data saved successfully!'
                        }).then(() => {
                            window.location.href = '{{route("user.index")}}';
                        });;
                    },
                    error: function(xhr, status, error) {
                        $('#loadingSpinner').addClass('hidden');
                        var response = JSON.parse(xhr.responseText);

                        var errorMessages = [];
                        if (response.data) {
                            for (var key in response.data) {
                                if (response.data.hasOwnProperty(key)) {
                                    errorMessages.push(response.data[key].join(' '));
                                }
                            }
                        }

                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            html: errorMessages.join('<br>')
                        });
                    }
                });
            });
        });
    </script>
@endsection
