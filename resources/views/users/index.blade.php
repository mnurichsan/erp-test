@extends('layouts.app')
@section('content')

<div class="card w-full md:w-auto bg-base-100 shadow-xl">
    <div class="card-body">
      <h2 class="card-title">Users</h2>
      <p><a href="{{route('user.create')}}" class="btn btn-primary" >Add Users</a></p>
      <div class="overflow-x-auto mt-4 w-full min-w-screen">
        <table class="table-lg" id="tUsers">
          <thead>
            <tr>
              <th>No.</th>
              <th>Name</th>
              <th>Email</th>
              <th>Address</th>
              <th>Date Of Birth</th>
              <th>Place Of Birth</th>
              <th>Age</th>
              <th>Action</th>
            </tr>
          </thead>

        </table>
      </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        var table = $('#tUsers').DataTable({
            "processing": true,
            "ajax": {
                "url": "http://127.0.0.1:8000/api/user"
            },
            "language": {
                "processing": "Loading data, please wait..."
            },
            "columns": [
                { "data": null },
                { "data": "name" },
                { "data": "email" },
                { "data": "address" },
                { "data": "date_of_birth" },
                { "data": "place_of_birth" },
                { "data": "age" },
                {
                    "data": null,
                    "defaultContent": "<button class='btn btn-warning edit-btn'>Edit</button> <button class='btn btn-error delete-btn' >Delete</button>"
                }
            ],
            "rowCallback": function(row, data, index) {
                $('td:eq(0)', row).html(index + 1);
            }
        });


        $('#tUsers tbody').on('click', '.edit-btn', function() {
            var data = table.row($(this).parents('tr')).data();
            var editUrl = "{{ route('user.edit', ':id') }}";
            editUrl = editUrl.replace(':id', data.id);
            window.location.href = editUrl;
        });


        $('#tUsers tbody').on('click', '.delete-btn', function() {
            var data = table.row($(this).parents('tr')).data();
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: 'http://127.0.0.1:8000/api/user/delete',
                        type: 'POST',
                        contentType: 'application/json',
                        data: JSON.stringify({ id: data.id }),
                        success: function(result) {
                            table.ajax.reload();
                            Swal.fire(
                                'Deleted!',
                                'Your record has been deleted.',
                                'success'
                            );
                        },
                        error: function(xhr, status, error) {
                            Swal.fire(
                                'Error!',
                                'There was a problem deleting your record.',
                                'error'
                            );
                        }
                    });
                }
            });
        });

    });
</script>

@endsection
