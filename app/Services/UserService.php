<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
class UserService
{
    public function __construct(
        private UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }


    public function get(){
        $data =  $this->userRepository->getAllData([],"id","ASC");

        $response = [
            "data" => $data,
            "message" => "Get User Succesfully",
            "status" => true
        ];

        return response()->json($response,200);
    }

    public function store($request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string|min:8',
            'date_of_birth' => 'date|nullable'
        ]);

        if ($validator->fails()) {
            $response = [
                "message" => "bad request, error validation",
                "status" => false,
                "data" => $validator->errors()
            ];
            return response()->json($response,400);
        }

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'address' => $request->address,
            'date_of_birth' => $request->date_of_birth,
            'place_of_birth' => $request->place_of_birth,
            'age' => $request->age
        ];

        $this->userRepository->storeData($data);

        $response = [
            "data" => $request->all(),
            "message" => "Store User Succesfully",
            "status" => true
        ];

        return response()->json($response,201);


    }

    public function detail($id){
        $data = $this->userRepository->find($id);

        $response = [
            "data" => $data,
            "message" => "Find User Succesfully",
            "status" => true
        ];

        return response()->json($response,200);

    }

    public function update($request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:users,id',
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'nullable|string|min:8',
            'date_of_birth' => 'date|nullable'
        ]);

        if ($validator->fails()) {
            $response = [
                "message" => "bad request, error validation",
                "status" => false,
                "data" => $validator->errors()
            ];
            return response()->json($response,400);
        }

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'address' => $request->address,
            'date_of_birth' => $request->date_of_birth,
            'place_of_birth' => $request->place_of_birth,
            'age' => $request->age
        ];

        $this->userRepository->updateWhere([['id',$request->id]],$data);

        $response = [
            "data" => $request->all(),
            "message" => "Update User Succesfully",
            "status" => true
        ];

        return response()->json($response,201);
    }


    public function delete($request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:users,id'
        ]);

        if ($validator->fails()) {
            $response = [
                "message" => "bad request, error validation",
                "status" => false,
                "data" => $validator->errors()
            ];
            return response()->json($response,400);
        }

        $this->userRepository->deleteWhere([['id',$request->id]]);

        $response = [
            "data" => $request->all(),
            "message" => "Delete User Succesfully",
            "status" => true
        ];

        return response()->json($response,200);

    }

}
