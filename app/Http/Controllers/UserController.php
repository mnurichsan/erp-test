<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct(
        private UserService $userService,
    )
    {
        $this->userService = $userService;
    }

    public function get(){
        return $this->userService->get();
    }

    public function store(Request $request){
        return $this->userService->store($request);
    }

    public function detail($id){
        return $this->userService->detail($id);
    }

    public function update(Request $request){
        return $this->userService->update($request);
    }

    public function delete(Request $request){
        return $this->userService->delete($request);
    }

}
