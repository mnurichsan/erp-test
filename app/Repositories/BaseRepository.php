<?php

namespace App\Repositories;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;



class BaseRepository
{

    /**
     * Model instance.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;


    /**
     * Constructor for BaseRepository.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get all records that match.
     *
     * @param array $where
     * @param string $sort_column
     * @param string $sort_order
     * @return \Illuminate\Support\Collection
     */
    public function getAllData($where = [],$sort_column="id", $sort_order = "ASC")
    {
        return $this->model
            ->where($where)
            ->orderBy($sort_column, $sort_order)
            ->get();
    }



    /**
     * Find a record by its ID.
     *
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function find($id)
    {
        return $this->model->find($id);
    }



    /**
     * Get a query builder instance for records that match the given conditions.
     *
     * @param array $where
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function whereData($where = [])
    {
        return $this->model->where($where);
    }

     /**
     * Delete Where a record by ID.
     *
     * @param array $where
     * @param int $userId
     * @return mixed
     */
    public function deleteWhere($where)
    {
        DB::beginTransaction();
        try {

            if(!$this->model->where($where)->count()) {
                throw new Exception("Data Not Found", 404);
            }

            $this->model->where($where)->delete();

            DB::commit();

            return true;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception($e->getMessage(), 500);
        }
    }


    /**
     * Update records that match the given conditions.
     *
     * @param array $where
     * @param array $data
     * @return mixed
     */
    public function updateWhere($where, $data)
    {
        DB::beginTransaction();
        try {

            if(!$this->model->where($where)->count()) {
                throw new Exception("Data Not Found", 404);
            }

            $data = $this->model->where($where)->update($data);
            DB::commit();

            return $data;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception($e->getMessage(), 500);
        }
    }


    /**
     * Store a new record
     *
     * @param array $data
     * @return mixed
     */
    public function storeData($data)
    {
        try {

            DB::beginTransaction();


            $this->model = $this->model->create($data);

            DB::commit();
            return $this->model;

        } catch(Exception $e) {

            DB::rollback();

            throw new Exception($e->getMessage(), 500);

        }
    }





}
